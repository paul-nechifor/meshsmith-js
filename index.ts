import { runCommand } from "./utils";
import fs from "fs";

type PathCommand =
  | ["M", number, number]
  | ["L", number, number]
  | ["Q", number, number, number, number]
  | ["C", number, number, number, number, number, number];

export abstract class Mesh {
  abstract getJson(): unknown;

  difference({ mesh }: { mesh: Mesh }) {
    return new DifferenceMesh({ mesh1: this, mesh2: mesh });
  }

  translate({ x = 0, y = 0, z = 0 }: { x?: number; y?: number; z?: number }) {
    return new TranslateMesh({ mesh: this, x, y, z });
  }

  build(): Mesh {
    buildMesh(this);
    return this;
  }
}

export async function buildMesh(mesh: Mesh): Promise<void> {
  const json = JSON.stringify(mesh.getJson());
  fs.writeFileSync("asdf.json", json, "utf8");
  const out = await runCommand("meshsmith-backend", [], json);
  process.stdout.write(out);
  process.exit(0);
}

export class DifferenceMesh extends Mesh {
  mesh1: Mesh;
  mesh2: Mesh;

  constructor({ mesh1, mesh2 }: { mesh1: Mesh; mesh2: Mesh }) {
    super();
    this.mesh1 = mesh1;
    this.mesh2 = mesh2;
  }

  getJson() {
    return {
      type: "Difference",
      mesh1: this.mesh1.getJson(),
      mesh2: this.mesh2.getJson(),
    };
  }
}

export class TranslateMesh extends Mesh {
  mesh: Mesh;
  x: number;
  y: number;
  z: number;

  constructor({
    mesh,
    x = 0,
    y = 0,
    z = 0,
  }: {
    x: number;
    y: number;
    z: number;
    mesh: Mesh;
  }) {
    super();
    this.mesh = mesh;
    this.x = x;
    this.y = y;
    this.z = z;
  }

  getJson() {
    return {
      type: "Translate",
      p: [this.x, this.y, this.z],
      mesh: this.mesh.getJson(),
    };
  }
}

export class ExtrudePathMesh extends Mesh {
  height: number;
  path: Path;

  constructor({ path, height }: { path: Path; height: number }) {
    super();
    this.height = height;
    this.path = path;
  }

  getJson() {
    return {
      type: "ExtrudePath",
      height: this.height,
      path: this.path.commands,
    };
  }
}

export class Path {
  commands: PathCommand[];
  constructor({ commands }: { commands: PathCommand[] }) {
    this.commands = commands;
  }

  extrude({ height }: { height: number }): Mesh {
    return new ExtrudePathMesh({ height, path: this });
  }

  static roundedRectangle({
    width,
    length,
    radius,
  }: {
    width: number;
    length: number;
    radius: number;
  }): Path {
    // TODO: Use proper circles, not Q's.
    if (radius * 2 > width || radius * 2 > length) {
      throw new Error(`The radius (${radius}) cannot be more than half the width (${width}) or length (${length}).`);
    }
    const x2 = width / 2;
    const x1 = x2 - radius;
    const y2 = length / 2;
    const y1 = y2 - radius;
    const commands: PathCommand[] = [
      ["M", x1, -y2],
      ["Q", x2, -y2, x2, -y1], // Bottom right corner.
    ];

    if (y1 > 0) {
      commands.push(["L", x2, y1]); // Right line.
    }

    commands.push(["Q", x2, y2, x1, y2]); // Top right corner.

    if (x1 > 0) {
      commands.push(["L", -x1, y2]); // Top line.
    }

    commands.push(["Q", -x2, y2, -x2, y1]); // Top left corner.

    if (y1 > 0) {
      commands.push(["L", -x2, -y1]); // Left line.
    }

    commands.push(["Q", -x2, -y2, -x1, -y2]); // Bottom left corner.

    // if (y1 > 0) {
    //   commands.push(["L", x1, -y2]); // Left line.
    // }

    return new Path({ commands });
  }
}
