import path from 'path';
import { spawn } from 'child_process';

export async function runCommand(
  command: string,
  args: string[],
  input?: string,
): Promise<string> {
  return new Promise((resolve, reject) => {
    const childProcess = spawn(command, args);

    let output = '';
    childProcess.stdout.on('data', (data) => {
      output += data.toString();
    });

    childProcess.stderr.on('data', (data) => {
      reject(data.toString());
    });

    childProcess.on('close', (code) => {
      if (code === 0) {
        resolve(output);
      } else {
        reject(new Error(`Process exited with code ${code}.`));
      }
    });

    if (input) {
      childProcess.stdin.write(input);
      childProcess.stdin.end();
    }
  });
}
