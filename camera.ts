import { Path } from "./index";

const length = 2;
const width = 5;
const height = 3;
const radius = 1;

function obj(thickness: number) {
  return Path.roundedRectangle({
    width: width - 2 * thickness,
    length: length - 2 * thickness,
    radius: radius - 2 * thickness,
  }).extrude({ height });
}

obj(0.1)
  .difference({ mesh: obj(0.2).translate({ z: 0.2 }) })
  .build();

const thickness = 0.2;
const body = Path.roundedRectangle({ width, length, radius })
  .extrude({ height })
  .difference({
    mesh: Path.roundedRectangle({
      width: width - 2 * thickness,
      length: length - 2 * thickness,
      radius: radius - 2 * thickness,
    })
      .extrude({ height })
      .translate({ z: thickness }),
  });

body.build();
